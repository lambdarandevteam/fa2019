<?php 
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function body(){
		$this->load->view('body');
	}

	function detailPost(){
		$this->load->view('post-detail');
	}
	function index(){
        $this->load->view("index");
    }
    function  listProvider(){
        $this->load->view("provider/list_index");
    }
    public function acceuilProduit(){
        //echo "ato body";
        $this->load->view('produit/produitIndex');
    }
}