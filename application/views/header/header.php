
    <div class="row">
        <div class="col-lg-12 p-0">
            <div class="row">
                <div class="col-lg-4"><img class="d-block w-100" style="width:100%;height:auto" src="<?php echo base_url('assets/images/images/logo_logiciel.png')?>" alt="headers"></div>
                <div class="col-lg-4 p-5">
                    <p>Slogan</p>
                </div>
                <div class="col-lg-4 p-5">
                    <span id="div_langue" style="position: absolute;top: 0;right: 30px">
                        <div id="google_translate_element" style="padding-left: 15px; padding-top: 13px"></div><script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'mg,ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
                            }
                        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    </span>
                    <div class="search-container text-center">
                        <form action="#">
                            <input type="text" placeholder="Search.." name="search">
                            <button type="submit"><i class="fa fa-search"></i>Recherche</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 p-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?php echo base_url('assets/images/images/headers.jpg')?>" alt="headers">
                    </div>
                    <div class="carousel-item">
                        <div class="texte">L’arachide à Madagascar, une filière prometteuse mais fragile</div>
                        <img class="d-block w-100" src="<?php echo base_url('assets/images/images/headers_arachide.jpg')?>" alt="Arachide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?php echo base_url('assets/images/images/headers_default.jpg')?>" alt="default">
                    </div>
                    <div class="carousel-item">
                        <div class="texte">Le black eyes (encore appelé niébé ou cornille) est un légumineux très apprécié ses apports nutritionnels.</div>
                        <img class="d-block w-100" src="<?php echo base_url('assets/images/images/headers_grainsec.jpg')?>" alt="Grain Sec">
                    </div>
                    <div class="carousel-item">
                        <div class="texte">Troisième producteur mondial de litchis, Madagascar enregistre annuellement 100 000 tonnes de ce fruit tropical.</div>
                        <img class="d-block w-100" src="<?php echo base_url('assets/images/images/headers_litchis.jpg')?>" alt="litchis">
                    </div>
                    <div class="carousel-item">
                        <div class="texte">la culture du riz est très importante à Madagascar et la plupart de la population la pratique.</div>
                        <img class="d-block w-100" src="<?php echo base_url('assets/images/images/headers_taroka.jpg')?>" alt="Vary">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
<style>
    div form input {
        border-radius: 10px;
    }
    .carousel-item .texte {
        position:absolute;
        top:2em;
        color:white;
        font-size:2em;
        width:692px;
        border-radius: 10px 10px 10px 10px;
        text-align: center;
        background-color: rgba(0,0,0,0.4)/* même largeur que l’image */
    }
    .carousel{
        width: 100%;
        height: auto;
    }
    .carousel-inner img{
        width: 100%;
        height: auto;
    }
</style>
<script type="text/javascript">
    $('.carousel').carousel({
        interval: 2000
    })
</script>