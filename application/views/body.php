<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/style.css'; ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>
<style type="text/css">
	body{
		width: 100%;
		height: 100%;
		background-image: url('<?php echo base_url().'assets/images/back.jpg'; ?>');
		background-repeat: no-repeat;
		background-size: cover;
	}
</style>
<body>

	<div class="primary-content">
		<div class="content">
			<div class="actualite">
				<div class="actu-content">
					<div class="actu-title">
						<div class="actu-poster">
							<img class="profil" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
							<span>Rina Andriandraina</span> <span class="actu-date">
						- 28 Septembre 2019 à 11:01
					</span>
						</div>
						<div class="title">
							Le marché local de Tamatave
						</div>
					</div>
					<div class="actu-content-post">
						<div class="img-post">
							<img class="img-actu" src="<?php echo base_url().'assets/images/marche-local.jpg'; ?>">
						</div>
						<div class="post-content">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur inventore ut, eligendi quos doloremque sint quia veritatis, temporibus ad recusandae accusantium laudantium, error sunt saepe harum culpa impedit iste. Quaerat!
						</div>
					</div>
					<div class="comment">
						<a href="<?php echo site_url("Home/detailPost")?>"><i class="fas fa-search"></i></a>
					</div>
				</div>
				<div class="actu-content">
					<div class="actu-title">
						<div class="actu-poster">
							<img class="profil" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
							<span>Rina Andriandraina</span> <span class="actu-date">
						- 28 Septembre 2019 à 11:01
					</span>
						</div>
						<div class="title">
							Le marché local de Tamatave
						</div>
					</div>
					<div class="actu-content-post">
						<div class="img-post">
							<img class="img-actu" src="<?php echo base_url().'assets/images/marche-local.jpg'; ?>">
						</div>
						<div class="post-content">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur inventore ut, eligendi quos doloremque sint quia veritatis, temporibus ad recusandae accusantium laudantium, error sunt saepe harum culpa impedit iste. Quaerat!
						</div>
					</div>
					<div class="comment">
						<a href="<?php echo site_url("Home/detailPost")?>"><i class="fas fa-search"></i></a>
					</div>
				</div>
			</div>

		</div>
	</div>

	

	<script type="text/javascript">
		
	</script>

</body>
</html>