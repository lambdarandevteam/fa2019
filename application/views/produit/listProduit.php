<div class="container">
    <div class="row">
        <?php $this->load->view("content/left") ?>
        <div class="col-10 pt-4 mt-4 pb-4" style="background-color: rgba(222,222,222,.5)">
            <div class="row">
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/black_prod.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Black Eyes</div>
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('/assets/images/vanille_m.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Vanille</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('/assets/images/girofle.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Girofle</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('/assets/images/litchi.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Litchi</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('/assets/images/cacao_pro.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Cacao</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/cofe.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Café</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- parti 3 -->
            <div class="row mt-4">
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('/assets/images/arachide.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Arachide</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('/assets/images/miel.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Miel</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="prod" style="">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/tabia.jpg')?>" />
                        </div>
                        <div class="w-100" style="background-color: #339966">
                            <div class="text-center text-white">Tomate</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="contact_provider" style="display: none">
    <div>
        <h4 class="w-100 text-center pb-3">Contacter le fournisseur</h4>
        <label for="name">Nom</label>
        <input type="text" class="form-control" id="name">
        <label for="mail">Email</label>
        <input type="text" class="form-control" id="mail">
        <label for="message">Message</label>
        <textarea id="message" class="form-control"></textarea>
        <button class="btn btn-success">Envoyer</button>
    </div>
</div>