<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/style.css'; ?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
</head>
<style type="text/css">
	body{
		width: 100%;
		height: 100%;
		background-image: url('<?php echo base_url().'assets/images/back.jpg'; ?>');
		background-repeat: no-repeat;
		background-size: cover;
        background-position: fixed;
	}
</style>
<body>
<?php $this->load->view('header/header');?>
<div class="container">
    <div class="row">
        <?php $this->load->view('content/left') ?>
        <div class="col-lg-10 mt-4">
            <div class="primary-content">
                <div class="content">
                    <div class="actualite-detail">
                        <div class="actu-title">
                            <div class="actu-poster">
                                <img class="profil-detail" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
                                <span>Rina Andriandraina</span> <span class="actu-date">
					- 28 Septembre 2019 à 11:01
				</span>
                            </div>
                            <div class="title-detail">
                                Le marché local de Tamatave
                            </div>
                        </div>
                        <div class="actu-content-post-detail">
                            <div class="img-post-detail">
                                <img class="img-actu-detail" src="<?php echo base_url().'assets/images/marche-local.jpg'; ?>">
                            </div>
                            <div class="post-content-detail">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur inventore ut, eligendi quos doloremque sint quia veritatis, temporibus ad recusandae accusantium laudantium, error sunt saepe harum culpa impedit iste. Quaerat!
                            </div>
                        </div>

                        <div class="comment-post">
                            <div class="comment-list">
                                <div class="comment-content">
                                    <div class="profil-comment">
                                        <div class="profil-comment-img">
                                            <img class="profil-comment-post" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
                                        </div>
                                        <div class="nom-comment">
                                            Willam Arthur
                                        </div>
                                    </div>
                                    <div class="comment-content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa rerum, fugiat laudantium. Corporis delectus nam quas! Numquam ad libero, ea cum molestiae laboriosam, ex dignissimos, blanditiis, ducimus odio quae esse.
                                    </div>
                                </div>
                                <div class="comment-date">
                                    28 Septembre 2019 - 14h00
                                </div>
                            </div>
                            <div class="comment-list-2">
                                <div class="comment-content">
                                    <div class="profil-comment">
                                        <div class="profil-comment-img">
                                            <img class="profil-comment-post" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
                                        </div>
                                        <div class="nom-comment">
                                            Johan Randria
                                        </div>
                                    </div>
                                    <div class="comment-content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa rerum, fugiat laudantium. Corporis delectus nam quas! Numquam ad libero, ea cum molestiae laboriosam, ex dignissimos, blanditiis, ducimus odio quae esse.
                                    </div>
                                </div>
                                <div class="comment-date">
                                    28 Septembre 2019 - 14h00
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="comment-content">
                                    <div class="profil-comment">
                                        <div class="profil-comment-img">
                                            <img class="profil-comment-post" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
                                        </div>
                                        <div class="nom-comment">
                                            Mamitiana Ramaroson
                                        </div>
                                    </div>
                                    <div class="comment-content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa rerum, fugiat laudantium. Corporis delectus nam quas! Numquam ad libero, ea cum molestiae laboriosam, ex dignissimos, blanditiis, ducimus odio quae esse.
                                    </div>
                                </div>
                                <div class="comment-date">
                                    28 Septembre 2019 - 14h00
                                </div>
                            </div>

                            <div class="champs-commentaire">
                                <span class="mettre-commentaire">Mettez ici votre commentaire ....</span>
                                <div class="commentaire-to-fill">
                                    <div class="zone">
                                        <input type="" class="form-control" name="">
                                    </div>
                                    <input type="button" class="bouton btn btn-primary" name="" value="Poster">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer/footer'); ?>
	<script type="text/javascript">
		
	</script>

</body>
</html>