<div class="col-lg-2 pl-0 pl-sm-0 mt-4">
    <div class="w-100">
        <div class="navbar navbar-dark" style="background-color: #f48135">
            <span style="color: white">Menu</span>
        </div>
        <div class="" id="" style="background-color: #339966;">
            <a href="<?php echo base_url('/index.php/Home')?>"><div style="border: solid;border-color: #339966;color: white" class="w-100 pl-2">Accueil</div></a>
            <div id="show_product" style="border: solid;border-color: #339966;color: white" class="w-100 pl-2">Produits</div>
            <div id="product_to_show" class="product_list pl-4" style="display: none">
                <a href="<?php echo base_url('/index.php/Home/acceuilProduit')?>"><div class="w-100" style="color: white">Black Eyes</div></a>
                <a href="<?php echo base_url('/index.php/Home/acceuilProduit')?>"><div class="w-100" style="color: white">Vanille</div></a>
                 <a href="<?php echo base_url('/index.php/Home/acceuilProduit')?>"><div class="w-100" style="color: white">Grofle</div></a>
                 <a href="<?php echo base_url('/index.php/Home/acceuilProduit')?>"><div class="w-100" style="color: white">Cacao</div></a>
                 <a href="<?php echo base_url('/index.php/Home/acceuilProduit')?>"><div class="w-100" style="color: white">Litchi</div></a>
                <a href="<?php echo base_url('/index.php/Home/acceuilProduit')?>"><div class="w-100" style="color: white">Café</div></a>
            </div>
            <a href="<?php echo base_url('/index.php/Provider/listProvider')?>"><div style="border: solid;border-color: #339966;color: white" class="w-100 pl-2">Fournisseurs</div></a>
        </div>
    </div>
    <div class="w-100 mt-4">
        <div class="navbar navbar-dark" style="background-color: #f48135">
            <span style="color: white">Infos</span>
        </div>
        <div class="" id="" style="background-color: #339966;">
            <img style="width: 100%" src="<?php echo base_url('/assets/images/infos_litchi.jpg')?>">
        </div>
        <div class="" id="" style="background-color: #339966;">
            <p class="p-2 text-white">La récolte de litchi 2019 approche à grand pas.Une prospection sera programmée le 29 septembre afin de donner des précisions sur la période optimale de récolte</p>
        </div>
    </div>
</div>
<input type="hidden" value="0" id="isProductShown">
<script type="text/javascript">
    $(document).ready(function() {
        $("#show_product").click(function() {
           var val = $("#isProductShown").val();
           if (val === "0"){
               $("#product_to_show").css("display","block");
               $("#isProductShown").val("1");
           } else{
               $("#product_to_show").css("display","none");
               $("#isProductShown").val("0");
           }
        });
    });
</script>