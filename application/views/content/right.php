<div class="col-lg-10 mt-4">
    <div class="primary-content">
        <div class="content">
            <div class="actualite">
                <div class="actu-content">
                    <div class="actu-title">
                        <div class="actu-poster">
                            <img class="profil" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
                            <span>Rina Andriandraina</span> <span class="actu-date">
						- 28 Septembre 2019 à 11:01
					</span>
                        </div>
                        <div class="title">
                            Le marché local de Tamatave
                        </div>
                    </div>
                    <div class="actu-content-post">
                        <div class="img-post">
                            <img class="img-actu" src="<?php echo base_url().'assets/images/marche-local.jpg'; ?>">
                        </div>
                        <div class="post-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur inventore ut, eligendi quos doloremque sint quia veritatis, temporibus ad recusandae accusantium laudantium, error sunt saepe harum culpa impedit iste. Quaerat!
                        </div>
                    </div>
                    <div class="comment">
                        <a href="<?php echo site_url("Home/detailPost")?>"><i class="fas fa-search"></i></a>
                    </div>
                </div>
                <div class="actu-content">
                    <div class="actu-title">
                        <div class="actu-poster">
                            <img class="profil" src="<?php echo base_url().'assets/images/profil-vide.jpg'; ?>">
                            <span>Rina Andriandraina</span> <span class="actu-date">
						- 28 Septembre 2019 à 11:01
					</span>
                        </div>
                        <div class="title">
                            Le marché local de Tamatave
                        </div>
                    </div>
                    <div class="actu-content-post">
                        <div class="img-post">
                            <img class="img-actu" src="<?php echo base_url().'assets/images/marche-local.jpg'; ?>">
                        </div>
                        <div class="post-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur inventore ut, eligendi quos doloremque sint quia veritatis, temporibus ad recusandae accusantium laudantium, error sunt saepe harum culpa impedit iste. Quaerat!
                        </div>
                    </div>
                    <div class="comment">
                        <a href="<?php echo site_url("Home/detailPost")?>"><i class="fas fa-search"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>