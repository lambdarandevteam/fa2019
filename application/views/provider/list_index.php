<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="fr">
<title></title>
<head>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<style type="text/css">
    .link:hover{
        cursor: pointer;
    }
    body{
        width: 100%;
        height: 100%;
        background-image: url('<?php echo base_url().'assets/images/back.jpg'; ?>');
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
</head>
<body>
<?php
$this->load->view("header/header");
$this->load->view("provider/list");
$this->load->view("footer/footer");

?>
</body>
</html>
