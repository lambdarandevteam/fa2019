<div class="container">
    <div class="row">
        <?php $this->load->view("content/left") ?>
        <div class="col-10 pb-4 mt-4" style="background-color: rgba(255,255,255,0.5);">
            <div class="row mt-4">
                <div class="col-lg-4">
                    <div style=" ">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/vanille_prov.jpg')?>" />
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <div class="text-center text-white">Fournisseur de vanille</div>
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <p class="text-center text-white">
                                Fournisseur de vanille provenant du Nord de Madagascar
                            </p>
                        </div>
                        <div class="w-100 mt-2" style="">
                            <div data-fancybox="" data-animation-duration="500" data-src="#contact_provider" class="link text-center" style="height: 30px;background-color: #f48135;color: white">Contacter</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style=" ">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/girofle_pro.jpg')?>" />
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <div class="text-center text-white">Fournisseur de girofle</div>
                            <p class="text-center text-white">
                                Fournisseur de girofle provenant du d’Analanjrofo
                            </p>
                        </div>
                        <div class="w-100 mt-2" style="">
                            <div data-fancybox="" data-animation-duration="500" data-src="#contact_provider" class="link text-center" style="height: 30px;background-color: #f48135;color: white">Contacter</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style=" ">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/cacao_pro.jpg')?>" />
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <div class="text-center text-white">Fournisseur de cacao</div>
                            <p class="text-center text-white">
                                Fournisseur de Cacao provenant de Diana
                            </p>
                        </div>
                        <div class="w-100 mt-2" style="">
                            <div data-fancybox="" data-animation-duration="500" data-src="#contact_provider" class="link text-center" style="height: 30px;background-color: #f48135;color: white">Contacter</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div style=" ">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/litchi_pro.jpg')?>" />
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <div class="text-center text-white">Fournisseur de litchi</div>
                            <p class="text-center text-white">
                                Fournisseur de litchi provenant de l'Est de Madagascar
                            </p>
                        </div>
                        <div class="w-100 mt-2" style="">
                            <div data-fancybox="" data-animation-duration="500" data-src="#contact_provider" class="link text-center" style="height: 30px;background-color: #f48135;color: white">Contacter</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style=" ">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/black_prod.jpg')?>" />
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <div class="text-center text-white">Fournisseur de "Black eyes"</div>
                            <p class="text-center text-white">
                                Fournisseur de "Black eyes" provenant de de Madagascar
                            </p>
                        </div>
                        <div class="w-100 mt-2" style="">
                            <div data-fancybox="" data-animation-duration="500" data-src="#contact_provider" class="link text-center" style="height: 30px;background-color: #f48135;color: white">Contacter</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style=" ">
                        <div class="w-100" style="height: 150px;overflow: hidden">
                            <img style="width: 100%" class="img-fluid" src="<?php echo base_url('assets/images/cafe_pro.jpg')?>" />
                        </div>
                        <div class="w-100" style=";background-color: #339966">
                            <div class="text-center text-white">Fournisseur 1</div>
                            <p class="text-center text-white">
                                Fournisseur de "Black eyes" provenant de de Madagascar
                            </p>
                        </div>
                        <div class="w-100 mt-2" style="">
                            <div data-fancybox="" data-animation-duration="500" data-src="#contact_provider" class="link text-center" style="height: 30px;background-color: #f48135;color: white">Contacter</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="contact_provider" style="display: none">
    <div>
        <h4 class="w-100 text-center pb-3">Contacter le fournisseur</h4>
        <label for="name">Nom</label>
        <input type="text" class="form-control" id="name">
        <label for="mail">Email</label>
        <input type="text" class="form-control" id="mail">
        <label for="message">Message</label>
        <textarea id="message" class="form-control"></textarea>
        <button class="btn btn-success">Envoyer</button>
    </div>
</div>