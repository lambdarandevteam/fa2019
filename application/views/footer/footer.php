
	<style type="text/css">
		

.button {
  z-index: 99;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 40px;
  background: #339966;
  border-radius: 20px;
  color: #F5F5F5;;
  font-size: 20px;
  letter-spacing: 1px;
  font-weight: 200;
  cursor: pointer;
}

.social {
  opacity: 0;
  position: relative;
  margin: 8px;
  width: 40px;
  height: 40px;
  border-radius: 100%;
  display: inline-block;
  color: #F5F5F5;;
  font-size: 20px;
  text-align: center;
  i {
   margin-top: 10px;
  }
  
  a {
    color: #FFF;
  }
}

.twitter {
  background: #339966;
}

.facebook {
  background: #339966;
}

.google {
  background: #339966;
}

.clicked {
  opacity: 1;
  transition: 1.2s all ease;
  transform: translateY(56px);
}

.vertical-menu {
  width: 100%; 
}

.vertical-menu a {
  background:transparent; 
  color: #F5F5F5;; 
  display: block; 
  padding: 9px; 
  text-decoration: none; 
}

.vertical-menu a:hover {
  background-color: #339966; 
}

.vertical-menu a.active {
  background-color: #4CAF50; 
  color: white;
}
	</style>
    <div class=" mt-4 pt-4 pb-4" style="background-color: #F66D1F; color: #F5F5F5;">
<div class="row">
		<div class="col-lg-3">
				 <div class="container">
				  <div class="button">Partager</div>
				  <div class="social twitter"><a href="#"><img src="<?php echo base_url('assets/images/tweet.png') ?>" style="  width: 40px;height: 40px; border-radius: 100%;"></a></div>
				  <div class="social facebook"><a href="#"><img src="<?php echo base_url('assets/images/fb.png') ?>" style="  width: 40px;height: 40px; border-radius: 100%;"></a></div>
				  <div class="social google"><a href="#"><img src="<?php echo base_url('assets/images/insta.png') ?>" style="  width: 40px;height: 40px; border-radius: 100%;"></a></div>
				</div>

				<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
				  <defs>
				    <filter id="goo">
				      <feGaussianBlur in="SourceGraphic" stdDeviation="8" result="blur" />
				      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
				      <feBlend in="SourceGraphic" in2="goo" />
				    </filter>
				  </defs>
				</svg>
			<script >
				$(".button").click(function(){
  				$(".social.twitter").toggleClass("clicked");
  				$(".social.facebook").toggleClass("clicked");
				$(".social.google").toggleClass("clicked");
  		
				})
			</script>
		</div>

		<div class="col-lg-2">
			<div class="vertical-menu">
				  <a href="#">Nos Produit</a>
				  <a href="#">Fournisseur</a>
				  <a href="#">FAQ</a>
				  <a href="#">Qui sommes-nous?</a>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="widget widget-text" style="margin-left: 20px;">
					<!-- <h3><img alt="logo blanc" height="44" src="" width="180"></h3> -->
				<h3 style="  font-size: 16px;">Besoin d’infos ?</h3>
				<p>N'hésitez pas à nous joindre du lundi au vendredi 9h-12h30 / 14h - 17h30</p>
				<p><span style="color: #339966;"><strong>01 23 45 67 89</strong></span> (appel gratuit)</p>
				<p class="address"><strong> <em class="fa fa-envelope-o fa-fw"></em></strong>| contact@madagricole.com</p>
				<!-- <p><em class="nc-icon-outline ui-1_email-85"></em> <a href="mailto:contact@vokatramalagasy">contact@vokatramalagasy</a></p>	 -->	        
			</div>
		</div>
			
		
		<div class="col-lg-4">
			<section class="contact-form">
	  			<form action="" method="post">
	    		<header>
	     			 <h1 class="h3 hidden-sm-down" style=" margin-left: 20px; font-size: 13px;">POSEZ VOTRE QUESTION</h1>
	    		</header>
	    		<section class="form-fields">
	        		<label>
	                    <select name="id_contact" style="margin-right: 100px; margin-left: 20px">
	                       <option value="2">Service client</option>
	                       <option value="1">Webmaster</option>
	                    </select>
	        		</label>

	        		<label>
	                    <input class="form-control" type="email" name="from" placeholder="Adresse Email" value="" style="margin-left: 20px; width: 200px;">
	        		</label>
	        		<label>
	                    <textarea cols="67" rows="3" name="message" placeholder="Message" class="form-control" style="margin-left: 20px; width: 200px; height: auto;"></textarea>
	        		</label>
	      		</section>
	
			      <footer class="form-footer">
			        <button type="submit" name="submitMessage" class="btn btn-success" style="margin-left: 20px">
			          Envoyer
			        </button>
			      </footer>
	      		</form>
			</section>
		</div>

</div>
        <div class="col-md-12 text-center text-white">
            <p> © 2019 - mada-agricole</p>
        </div>
    </div>

